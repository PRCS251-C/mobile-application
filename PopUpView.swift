//
//  PopUpView.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 22/03/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//

import UIKit

@IBDesignable class PopUpView: UIView {

    @IBDesignable class PopUpView: UIView {
        
        @IBInspectable var cornerRadius: CGFloat = 0 {
            didSet {
                self.layer.cornerRadius = cornerRadius
            }
        }
    }
}
