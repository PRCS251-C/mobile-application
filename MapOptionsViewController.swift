//
//  MapOptionsViewController.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 13/03/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//
import Foundation
import UIKit

protocol MapOptionsDelegate : class{
    func dismissAndUpdate()
}

class MapOptionsViewController: UITableViewController {

    
    @IBOutlet weak var backgroundUpdateSwitch: UISwitch!
    @IBOutlet weak var headingUpSwitch: UISwitch!
    @IBOutlet weak var showTrafficSwitch: UISwitch!
    @IBOutlet weak var GPSPrecisionLabel: UILabel!
    @IBOutlet weak var GPSPRecisionSlider: UISlider!
    @IBOutlet weak var doneButton: UIButton!
    
    weak var delegate : MapOptionsDelegate?
    let defaults = UserDefaults.standard
    
    let step: Float = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GPSPRecisionSlider.value = (Float(defaults.integer(forKey: "precision")))
        GPSPrecisionLabel.text = "\(Int(defaults.integer(forKey: "precision")))"
        
        if defaults.bool(forKey: "background") == true {
            backgroundUpdateSwitch.isOn = true
        }else{
            backgroundUpdateSwitch.isOn = false
        }
        
        if defaults.bool(forKey: "heading") == true {
            headingUpSwitch.isOn = true
        }else{
            headingUpSwitch.isOn = false
        }
        
        if defaults.bool(forKey: "traffic") == true {
            showTrafficSwitch.isOn = true
        }else{
            showTrafficSwitch.isOn = false
        }


        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func doBackgroundUpdates(_ sender: UISwitch) {
        if backgroundUpdateSwitch.isOn{
            defaults.set(true, forKey: "background")
        }else{
            backgroundUpdateSwitch.isOn = false
            defaults.set(false, forKey: "background")
        }
    }
    
    @IBAction func doHeadingUp(_ sender: UISwitch) {
        if headingUpSwitch.isOn{
            defaults.set(true, forKey: "heading")
        }else{
            headingUpSwitch.isOn = false
            defaults.set(false, forKey: "heading")
        }
    }
    
    @IBAction func doShowTraffic(_ sender: UISwitch) {
        if showTrafficSwitch.isOn{
            defaults.set(true, forKey: "traffic")
        }else{
            showTrafficSwitch.isOn = false
            defaults.set(false, forKey: "traffic")
        }
    }
    @IBAction func doGPSPrecisionSlider(_ sender: UISlider) {
        let roundedValue = round (sender.value / step) * step
        sender.value = roundedValue
        defaults.set(roundedValue, forKey: "precision")
        GPSPrecisionLabel.text = "\(Int(defaults.integer(forKey: "precision"))) %"
    }
    @IBAction func doDismissAndUpdate(_ sender: UIButton){
        self.delegate?.dismissAndUpdate()
    }
}
