//
//  ViewController.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 22/02/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//

import UIKit
import AudioToolbox

class ViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginbtn: UIButton!
    @IBOutlet weak var loginActivity: UIActivityIndicatorView!
    @IBOutlet weak var logActivity: UIActivityIndicatorView!
    
    var Username : String = ""
    var Password : String = ""
    
    override func shouldPerformSegue(withIdentifier identifier: String?, sender: Any? ) -> Bool {
        Username = userTextField.text!
        Password = passwordTextField.text!
        if userTextField.text == ""{
            messageLabel.text = "Incorrect Username or Password. Please Try Again."
            messageLabel.isHidden = false
            return false
        }else if let ident = identifier {
            if ident == "Logged" {
                if !ModelController.getInstance().login(username: Username, password: Password) {
                    messageLabel.text = "Incorrect Username or Password. Please Try Again."
                    messageLabel.isHidden = false
                    return false
                }
            }
        }
        messageLabel.isHidden = true
        return true
    }
    func dismissKeyboard(){
        userTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        userTextField.delegate = self
        passwordTextField.delegate = self
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: (#selector(ViewController.dismissKeyboard))))
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        userTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        return true
    }
}

