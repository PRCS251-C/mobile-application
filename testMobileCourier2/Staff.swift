//
//  Staff.swift
//  apiConnectionTest
//
//  Created by Alex on 02/05/2017.
//  Copyright © 2017 Alex Murphy. All rights reserved.
//

import Foundation

class Staff {
    
    private var staffId     : Int    = 0
    private var firstName   : String = ""
    private var lastName    : String = ""
    private var email       : String = ""
    private var phoneNumber : String = ""
    private var dateOfBirth : String = ""
    private var position    : String = ""
    private var salary      : Int    = 0
    private var username    : String = ""
    private var storeId     : Int    = 0
    private var password    : String = ""
    
    public init() {
    }
    
    public init(staffId : Int, firstName : String, lastName : String, email : String, phoneNumber : String, dateOfBirth : String, position : String, salary : Int, username : String, password : String, storeId : Int) {
        self.staffId        = staffId
        self.firstName      = firstName
        self.lastName       = lastName
        self.email          = email
        self.phoneNumber    = phoneNumber
        self.dateOfBirth    = dateOfBirth
        self.position       = position
        self.salary         = salary
        self.username       = username
        self.storeId        = storeId
        self.setPassword(password)
    }
    
    public func setPassword(_ pass : String) {
        self.password = hashPassword(pass)
    }
    
    public func getPassword() -> String {
        return self.password
    }
    
    public func hashPassword(_ pass : String) -> String {
        return pass.sha256
    }
}
