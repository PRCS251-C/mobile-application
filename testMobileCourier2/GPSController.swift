//
//  GPSController.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 24/04/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//

import Foundation
import MapKit

class GPSController: UIViewController, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        mapView.delegate = self
        
        let deliveryLocation = CLLocationCoordinate2D(latitude: 50.385313, longitude: -4.132238)
        let sourceLocation = CLLocationCoordinate2D(latitude: 50.374528, longitude: -4.140647)
        
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let deliveryPlacemark = MKPlacemark(coordinate: deliveryLocation, addressDictionary: nil)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let deliveryMapItem = MKMapItem(placemark: deliveryPlacemark)
        
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "Plymouth University"
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        let deliveryAnnotation = MKPointAnnotation()
        deliveryAnnotation.title = "3B Western College Road"
        
        if let location = deliveryPlacemark.location {
            deliveryAnnotation.coordinate = location.coordinate
        }
        
        self.mapView.showAnnotations([sourceAnnotation,deliveryAnnotation], animated: true )
        
        let directionRequest = MKDirectionsRequest()
        directionRequest.source = sourceMapItem
        directionRequest.destination = deliveryMapItem
        directionRequest.transportType = .any
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.add((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegionForMapRect(rect), animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.red
        renderer.lineWidth = 4.0
        
        return renderer
    }
}
