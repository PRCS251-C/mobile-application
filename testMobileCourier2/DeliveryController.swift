//
//  DeliveryController.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 22/03/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//

import Foundation
import UIKit
import AudioToolbox

class deliveryController: UIViewController{
    
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var availableSwitch: UISwitch!
    @IBOutlet weak var availableLabel: UILabel!
    @IBOutlet weak var deliveriesButton: UIButton!
    
    var feedItems: NSArray = NSArray()
    var selectedOrder : Order = Order()
    
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        var isAvailable = defaults.value(forKey: "availability") as! Bool
        
        ModelController.getInstance().getOrders()
        deliveriesButton.isEnabled = false
        
        if (isAvailable) {
            availableSwitch.isOn = true
            defaults.set(true, forKey: "availability")
            availableLabel.text = "Available"
            availableLabel.textColor = UIColor.green
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            deliveriesButton.isEnabled = true
        } else {
            availableSwitch.isOn = false
            defaults.set(false, forKey: "availability")
            availableLabel.text = "Not Available"
            availableLabel.textColor = UIColor.red
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            deliveriesButton.isEnabled = false
        }
    }
    
    @IBAction func switchValueChanged(_ sender: Any) {
        if availableSwitch.isOn{
            defaults.set(true, forKey: "availability")
            availableLabel.text = "Available"
            availableLabel.textColor = UIColor.green
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            deliveriesButton.isEnabled = true

        }else{
            availableSwitch.isOn = false
            defaults.set(false, forKey: "availability")
            availableLabel.text = "Not Available"
            availableLabel.textColor = UIColor.red
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            deliveriesButton.isEnabled = false
        }
    }
    
    func itemsDownloaded(items: NSArray){
        feedItems = items
    }
    
    @IBAction func LogOff(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "availability")
    }
}

