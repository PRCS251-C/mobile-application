//
//  MapController.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 03/03/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//
// -----------

import Foundation
import MapKit
import CoreLocation
import AudioToolbox

protocol mapControllerDelegate : class {
    func changeDetails(toValue:String)
    
    func receiveDetails() -> [String]
}

class MapController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, apiDelegate {
    
    @IBOutlet weak var deliverySuccessful: UIButton!
    @IBOutlet weak var deliveryUnsuccessful: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var declineButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var deliveryFullName: UILabel!
    @IBOutlet weak var deliveryContactDetails: UILabel!
    @IBOutlet weak var deliveryFullAddress: UILabel!
    var fullAddress: String!
    var customer: String!
    var customerNumber: String!
    var processing = false
    var wasPostSuccessful = false
    var orderId : Int? = 0
    weak var delegate : mapControllerDelegate?
    var order : Order!
    
    var defaults = UserDefaults.standard
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let value = delegate?.receiveDetails() ?? ["0"]
        print(value)
        orderId = Int((value[0]))
        print(orderId)
        deliverySuccessful.isEnabled = false;
        deliveryUnsuccessful.isEnabled = false;
        
        for item in ModelController.getInstance().getOrderArray() {
            if item.getOrderId() == orderId {
                order = item
                fullAddress = item.getDeliveryAddrsss()
                customer = item.getCustomerName()
                customerNumber = item.getCustomerPhone()
            }
        }
        
        deliveryFullAddress.text = fullAddress
        deliveryFullName.text = customer
        deliveryContactDetails.text = customerNumber
        
    }
    
    func receiveData(data: [[String : Any]]) {
        for item in data {
            for (key, content) in item {
                if key == "post" {
                    if let blnContent = content as? Bool {
                        wasPostSuccessful = blnContent
                        processing = false
                        deliveryUnsuccessful.isEnabled = true
                        deliverySuccessful.isEnabled = true
                    }
                }
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Accept") {
            processing = true;
            APIConnection.apiDelegate = self
            
            let json = ["ORDER_ID" : order.getOrderId(), "ORDER_PRICE" : order.getPrice(), "ORDER_TIME" : order.getOrderTime(), "ORDER_STATUS" : "Out For Delivery", "CUSTOMER_ID" : order.getCustomerId() ?? nil, "PAYMENT_ID" : order.getPaymentId() ?? nil, "STAFF_ID" : defaults.value(forKey: "staffId") ?? 1, "DELIVERY_ADDRESS" : order.getDeliveryAddrsss(), "DELIVERY_POSTCODE" : order.getDeliveryPostcode()]
            
            APIConnection.setDelivery(JSONData: json)
            
            var waiting = 0
            
            while (processing) {
                if (waiting < 10000000) {
                    waiting += 1
                } else {
                    break
                }
            }
            
            deliverySuccessful.isEnabled = true
            deliveryUnsuccessful.isEnabled = true
            
            print(wasPostSuccessful)
        } else if (segue.identifier == "Map") {
            defaults.set(order.getOrderId(), forKey: "orderId")
        } else if (segue.identifier == "deliverySuccessful") {
            processing = true;
            APIConnection.apiDelegate = self
            
            let json = ["ORDER_ID" : order.getOrderId(), "ORDER_PRICE" : order.getPrice(), "ORDER_TIME" : order.getOrderTime(), "ORDER_STATUS" : "Fulfilled", "CUSTOMER_ID" : order.getCustomerId() ?? nil, "PAYMENT_ID" : order.getPaymentId() ?? nil, "STAFF_ID" : defaults.value(forKey: "staffId") ?? 1, "DELIVERY_ADDRESS" : order.getDeliveryAddrsss(), "DELIVERY_POSTCODE" : order.getDeliveryPostcode()]
            
            APIConnection.setDelivery(JSONData: json)
            
            var waiting = 0
            
            while (processing) {
                if (waiting < 10000000) {
                    waiting += 1
                } else {
                    break
                }
            }
            
            print(wasPostSuccessful)
        } else if (segue.identifier == "deliveryUnsuccessful") {
            processing = true;
            APIConnection.apiDelegate = self
            
            let json = ["ORDER_ID" : order.getOrderId(), "ORDER_PRICE" : order.getPrice(), "ORDER_TIME" : order.getOrderTime(), "ORDER_STATUS" : "Issue Delivering", "CUSTOMER_ID" : order.getCustomerId() ?? nil, "PAYMENT_ID" : order.getPaymentId() ?? nil, "STAFF_ID" : defaults.value(forKey: "staffId") ?? 1, "DELIVERY_ADDRESS" : order.getDeliveryAddrsss(), "DELIVERY_POSTCODE" : order.getDeliveryPostcode()]
            
            APIConnection.setDelivery(JSONData: json)
            
            var waiting = 0
            
            while (processing) {
                if (waiting < 10000000) {
                    waiting += 1
                } else {
                    break
                }
            }
            
            print(wasPostSuccessful)
        }
    }

    @IBAction func declineDelivery(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
}
