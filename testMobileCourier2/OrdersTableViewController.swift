//
//  OrdersTableViewController.swift
//  testMobileCourier2
//
//  Created by Alex on 09/05/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//

import UIKit

class OrdersTableViewController: UITableViewController, mapControllerDelegate {
    
    var orders = ModelController.getInstance().getOrderArray()
    var orderIds = [Int]()
    var orderAddress = [String]()
    var orderCustomer = [String]()
    var orderPhone = [String]()
    var valueToPass : [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        while (ModelController.getInstance().working) {
            //sit and wait
        }
        
        orders = ModelController.getInstance().getOrderArray()
        
        for item in orders {
            orderIds.append(item.getOrderId())
            orderAddress.append(item.getDeliveryAddrsss())
            orderCustomer.append(item.getCustomerName())
            orderPhone.append(item.getCustomerPhone())
            
        }
        
        tableView.estimatedRowHeight = 50
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderIds.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrdersTableCell", for: indexPath) as! OrdersTableViewCell

        let row = indexPath.row
        
        cell.orderLabel.font = UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)
        
        cell.orderLabel.text = String(orderIds[row])
        cell.addressLabel1.text = String(orderAddress[row])
        

        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Delivery") {
            let destination = segue.destination as? MapController
            destination?.delegate = self
        }
        
        if let item = sender as? OrdersTableViewCell {
            print(item.orderLabel.text)
            valueToPass.removeAll()
            valueToPass.append(item.orderLabel.text ?? "0")
        }
    }
    
    func changeDetails(toValue: String) {
        
    }
    
    func receiveDetails() -> [String] {
        return self.valueToPass
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
