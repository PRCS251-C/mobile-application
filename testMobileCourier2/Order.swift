//
//  Order.swift
//  apiConnectionTest
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex Murphy. All rights reserved.
//

import Foundation

class Order {
    
    private var orderId          : Int
    private var price            : Double
    private var orderTime        : String
    private var status           : String
    private var customerId       : Int?
    private var paymentId        : Int?
    private var staffId          : Int?
    private var deliveryAddress  : String
    private var deliveryPostcode : String
    private var customerName     : String
    private var customerPhone    : String
    
    public init() {
        self.orderId    = 0
        self.price      = 0.00
        self.orderTime  = ""
        self.status     = ""
        self.customerId = 0
        self.paymentId  = 0
        self.staffId    = 0
        self.deliveryAddress = ""
        self.deliveryPostcode = ""
        self.customerName = ""
        self.customerPhone = ""
    }
    
    public init(orderId : Int, price : Double, orderTime : String, status : String, customerId : Int, paymentId : Int, staffId : Int, deliveryAddress : String, deliveryPostcode : String, customerName : String, customerPhone : String) {
        self.orderId    = orderId
        self.price      = price
        self.orderTime  = orderTime
        self.status     = status
        self.customerId = customerId
        self.paymentId  = paymentId
        self.staffId    = staffId
        self.deliveryAddress = deliveryAddress
        self.deliveryPostcode = deliveryPostcode
        self.customerName = customerName
        self.customerPhone = customerPhone
    }
    
    public func setOrderId(orderId : Int) {
        self.orderId = orderId
    }
    
    public func getOrderId() -> Int {
        return self.orderId
    }
    
    public func setPrice(price : Double) {
        self.price = price
    }
    
    public func getPrice() -> Double {
        return self.price
    }
    
    public func setOrderTime(orderTime : String) {
        self.orderTime = orderTime
    }
    
    public func getOrderTime() -> String {
        return self.orderTime
    }
    
    public func setStatus(status : String) {
        self.status = status
    }
    
    public func getStatus() -> String {
        return self.status
    }
    
    public func setCustomerId(customerId : Int?) {
        self.customerId = customerId
    }
    
    public func getCustomerId() -> Int? {
        return self.customerId
    }
    
    public func setPaymentId(paymentId : Int?) {
        self.paymentId = paymentId
    }
    
    public func getPaymentId() -> Int? {
        return self.paymentId
    }
    
    public func setStaffId(staffId : Int?) {
        self.staffId = staffId
    }
    
    public func getStaffId() -> Int? {
        return self.staffId
    }
    
    public func setDeliveryAddress(deliveryAddress : String) {
        self.deliveryAddress = deliveryAddress
    }
    
    public func getDeliveryAddrsss() -> String {
        return self.deliveryAddress
    }
    
    public func setDeliveryPostcode(deliveryPostcode : String) {
        self.deliveryPostcode = deliveryPostcode
    }
    
    public func getDeliveryPostcode() -> String {
        return self.deliveryPostcode
    }
    
    public func setCustomerName(customerName : String) {
        self.customerName = customerName
    }
    
    public func getCustomerName() -> String{
        return self.customerName
    }
    
    public func setCustomerPhone(customerPhone : String ){
        self.customerPhone = customerPhone
    }
    
    public func getCustomerPhone() -> String {
        return self.customerPhone
    }
}
