//
//  APIConnection.swift
//  testMobieCourier2
//
//  Created by Alex on 27/02/2017.
//  Copyright © 2017 Alex Murphy. All rights reserved.
//

import Foundation

protocol googleDirectionsDelegate : class {
    func receiveDirections(directions : [String : Any])
}

protocol apiDelegate : class {
    func receiveData(data : [[String : Any]])
}

class APIConnection {
    
    static weak var delegate : googleDirectionsDelegate?
    static weak var apiDelegate : apiDelegate?
    
    static func getOrderData() {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/Order/AwaitingDelivery")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            if error != nil {
                print("error")
            } else {
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments, .mutableContainers]) as? [[String : Any]] else {
                        print("Error parsing JSON")
                        return
                    }
                    
                    DispatchQueue.global().async {
                        apiDelegate?.receiveData(data: [["order" : json]])
                    }
                    
                } catch {
                    print("Error reading JSON")
                }
            }
        }
        
        task.resume()
    }
    
    static func getCustomerData(id : Int) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/Customer/\(id)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            if error != nil {
                print("error")
            } else {
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments, .mutableContainers]) as? [String : Any] else {
                        print("Error parsing JSON")
                        return
                    }
                    
                    DispatchQueue.global().async {
                        apiDelegate?.receiveData(data: [["customer" : [json]]])
                    }
                    
                } catch {
                    print("Error reading JSON")
                }
            }
        }
        
        task.resume()
    }
    
    static func getDirections(_ uri : String) {
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string : uri)
        var request = URLRequest(url: url!)
        request.httpMethod = "GET"
        var dispatched : Bool = false;
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            if error == nil {
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments, .mutableContainers]) as? [String : Any] else {
                        print("Error parsing JSON")
                        return
                    }
                    
                    APIConnection.printJsonArray(json: [json])
                    
                    DispatchQueue.global().async {
                        print("Dispatch with JSON")
                        dispatched = true
                        delegate?.receiveDirections(directions: json)
                        return
                    }
                    
                } catch {
                    print("Error caught parsing JSON")
                }
                print(error?.localizedDescription as Any)
            }
            
            if (!dispatched) {
                DispatchQueue.global().async {
                    print("Dispatch without JSON")
                    //delegate?.receiveDirections(directions: nil)
                }
            }
            
        }
        
        task.resume()
        
    }
    
    static func postData(table : String, JSONData params : [String : Any]) {
        let url = URL(string: "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/\(table)")!
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] {
                    DispatchQueue.global().async {
                        print(json)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    static func printJsonArray(json : [[String : Any]]) {
        for item in json {
            for (key, data) in item {
                print("\(key) : \(data)")
            }
        }
    }
    
    static func getSalt(username: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/Staff/\(username)/Salt")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            if error != nil {
                print("error")
            } else {
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments, .mutableContainers]) as? [String] else {
                        print("Error parsing JSON")
                        return
                    }
                    
                    if json.count != 0 {
                        let jsonData = [["salt" : json[0]]]
                        
                        DispatchQueue.global().async {
                            apiDelegate?.receiveData(data: jsonData)
                        }
                    }
                    
                } catch {
                    print("Error reading JSON")
                }
            }
        }
        
        task.resume()
    }
    
    static func login(JSONData params : [String : Any]) {
        let url = URL(string: "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/Staff/Login")!
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest) {
            (_, response, _) in
            
            if let httpResponse = response as? HTTPURLResponse {
                DispatchQueue.global().async {
                    if (httpResponse.statusCode >= 200 && httpResponse.statusCode < 300) {
                        apiDelegate?.receiveData(data: [["login" : true]])
                    } else {
                        apiDelegate?.receiveData(data: [["login" : false]])
                    }
                }
            }
        }
        
        task.resume()
    }
    
    static func setDelivery(JSONData params : [String : Any]) {
        print(params)
        var orderId : Int = params["ORDER_ID"] as! Int
        let url = URL(string: "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/Order/\(orderId)")!
        print(url)
        
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            guard error == nil else {
                return
            }
            
            guard let data = data else {
                return
            }
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String : Any] {
                    DispatchQueue.global().async {
                        print(json)
                        apiDelegate?.receiveData(data: [["post":true]])
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        
        task.resume()
    
    }

    static func getStaffId(username: String) {
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: "http://xserve.uopnet.plymouth.ac.uk/Modules/INTPROJ/PRCS251C/api/Staff/username/\(username)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = session.dataTask(with: request as URLRequest) {
            (data, response, error) in
            
            if error != nil {
                print("error")
            } else {
                do {
                    guard let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments, .mutableContainers]) as? [[String : Any]] else {
                        print("Error parsing JSON")
                        return
                    }
                    
                    var jsonData : [[String : Any]]!
                    
                    if json.count != 0 {
                        for item in json {
                            for (key, data) in item {
                                if key == "STAFF_ID" {
                                    jsonData = [["staffId" : data]]
                                }
                            }
                            
                        }
                        
                        DispatchQueue.global().async {
                            apiDelegate?.receiveData(data: jsonData)
                        }
                    }
                    
                } catch {
                    print("Error reading JSON")
                }
            }
        }
        
        task.resume()
    }
}
