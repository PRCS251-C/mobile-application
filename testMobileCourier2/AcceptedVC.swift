//
//  AcceptedVC.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 22/03/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//

import UIKit

class AcceptedVC: UIViewController {
    
    let defaults = UserDefaults.standard

    override func viewDidLoad() {
        super.viewDidLoad()

      
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissPopUp(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    
    @IBAction func dismissOK(_ sender: UIButton) {
        dismiss(animated: true)
        
    }
}
