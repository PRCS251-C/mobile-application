//
//  ModelController.swift
//  apiConnectionTest
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex Murphy. All rights reserved.
//

import Foundation

class ModelController : apiDelegate {
    
    private static let instance : ModelController = ModelController()
    
    var defaults = UserDefaults.standard
    
    private var salt = ""
    private var requestPending = false;
    private var loginResult = false;
    private var customerData : [[String : Any]] = []
    
    public var working = false;
    
    private var orderArray = [Order]()
    
    private init() {}
    
    public static func getInstance() -> ModelController {
        return instance
    }
    
    public func addOrder(order : Order) {
        self.orderArray.append(order)
    }
    
    public func removeOrder(order : Order) {
        if let index = self.orderArray.index(where: { (item) -> Bool in
            item.getOrderId() == order.getOrderId()}) {
            self.orderArray.remove(at: index)
        }
    }
    
    public func getOrderArray() -> [Order] {
        return self.orderArray
    }
    
    public func getOrders() {
        self.orderArray.removeAll()
        APIConnection.apiDelegate = self
        APIConnection.getOrderData()
    }
    
    public func login(username: String, password: String) -> Bool {
        var waiting = 0
        requestPending = true;
        getSalt(username: username)
        
        while (requestPending) {
            if (waiting < 10000000000) {
                waiting += 1
            } else {
                break
            }
        }
        
        let saltedPassword = password + salt
        
        waiting = 0
        requestPending = true;
        loginResult = false;
        APIConnection.login(JSONData: ["username" : username, "password" : saltedPassword.sha256])
        
        while (requestPending) {
            if (waiting < 10000000000) {
                waiting += 1
            } else {
                break
            }
        }
        
        print(loginResult)
        
        waiting = 0
        requestPending = true;
        APIConnection.apiDelegate = self
        APIConnection.getStaffId(username: username)
        
        while (requestPending) {
            if (waiting < 1000000000) {
                waiting += 1
            } else {
                break
            }
        }
        
        return loginResult
    }
    
    public func getSalt(username : String) {
        APIConnection.apiDelegate = self
        APIConnection.getSalt(username: username)
    }
    
    public func processOrders(json : [[String : Any]]) {
        working = true;
        for item in json {
            let order = Order()
            if let orderId = item["ORDER_ID"] as? Int {
                order.setOrderId(orderId: orderId)
            }
            if let orderTime = item["ORDER_TIME"] as? String {
                order.setOrderTime(orderTime: orderTime)
            }
            if let orderPrice = item["ORDER_PRICE"] as? Double {
                order.setPrice(price: orderPrice)
            }
            if let orderStatus = item["ORDER_STATUS"] as? String {
                order.setStatus(status: orderStatus)
            }
            if let orderTime = item["ORDER_TIME"] as? String {
                order.setOrderTime(orderTime: orderTime)
            }
            if let deliveryAddress = item["DELIVERY_ADDRESS"] as? String {
                order.setDeliveryAddress(deliveryAddress: deliveryAddress)
            }
            if let deliveryPostcode = item["DELIVERY_POSTCODE"] as? String {
                order.setDeliveryPostcode(deliveryPostcode: deliveryPostcode)
            }
            if let customerId = item["CUSTOMER_ID"] as? Int {
                order.setCustomerId(customerId: customerId)
            } else {
                order.setCustomerId(customerId: nil)
            }
            if let paymentId = item["PAYMENT_ID"] as? Int {
                order.setPaymentId(paymentId: paymentId)
            } else {
                order.setPaymentId(paymentId: nil)
            }
            
            requestPending = true
            APIConnection.getCustomerData(id: order.getCustomerId()!)
            var waiting = 0
            
            while (requestPending) {
                if (waiting < 10000000) {
                    waiting += 1
                } else {
                    break
                }
            }
            
            if (!customerData.isEmpty) {
                for content in customerData {
                    var firstName = ""
                    var surname = ""
                    if let customerFirstName = content["CUSTOMER_FIRST_NAME"] as? String {
                        firstName = customerFirstName
                    }
                    if let customerSurname = content["CUSTOMER_LAST_NAME"] as? String {
                        surname = customerSurname
                    }
                    let name = firstName + " " + surname
                    order.setCustomerName(customerName: name)
                    
                    if let customerPhoneNumb = content["CUSTOMER_PHONE_NUMB"] as? String {
                        order.setCustomerPhone(customerPhone: customerPhoneNumb)
                    }
                }
            }
            
            customerData = []
            
            self.addOrder(order: order)
            print(order.getOrderId())
            print(order.getDeliveryAddrsss())
            print(item["DELIVERY_ADDRESS"])
        }
        working = false;
    }
    
    public func receiveData(data: [[String : Any]]) {
        for item in data {
            for (key, content) in item {
                if key == "salt" {
                    salt = content as! String
                    requestPending = false;
                } else if key == "login" {
                    if content as! Bool {
                        loginResult = true;
                        requestPending = false;
                    } else {
                        loginResult = false;
                        requestPending = false;
                    }
                } else if key == "order" {
                    if let orderData = content as? [[String : Any]] {
                        processOrders(json: orderData)
                        requestPending = false;
                    }
                    
                } else if key == "customer" {
                    if let customerData = content as? [[String : Any]] {
                        self.customerData = customerData
                    }
                } else if key == "staffId" {
                    defaults.set(content, forKey: "staffId")
                    requestPending = false
                }
            }
        }
    }
}

extension Data {
    var hexString : String {
        let string = self.map{Int($0).hexString}.joined()
        print(string)
        return string
    }
    
    var sha256 : Data {
        var result = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        _ = result.withUnsafeMutableBytes {resultPtr in
            self.withUnsafeBytes {(bytes: UnsafePointer<UInt8>) in
                CC_SHA256(bytes, CC_LONG(count), resultPtr)
            }
        }
        
        return result
    }
}

extension Int {
    var hexString : String {
        return String(format: "%02x", self)
    }
}

extension String {
    var hexString : String {
        return self.data(using: .utf8)!.hexString
    }
    
    var sha256 : String {
        return self.data(using: .utf8)!.sha256.hexString
    }
}
