//
//  GPSController.swift
//  testMobileCourier2
//
//  Created by Daniel Warren on 25/04/2017.
//  Copyright © 2017 Daniel Warren. All rights reserved.
//

import Foundation
import CoreLocation
import PXGoogleDirections

class GPSController: UIViewController, CLLocationManagerDelegate, googleDirectionsDelegate{
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var addressLabel: UILabel!
    
    let defaults = UserDefaults.standard
    
    var order : Order!

    let locationManager = CLLocationManager()
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?origin=24+James+Street+Plymouth+UK&destination=3+Western+College+Road+Plymouth+UK&key=AIzaSyB_iaajdIoZeqVM56ecblrIz_NJWj2ERMo&avoid=highways&mode=bicycling"
    var directions: [String : Any] = [:]
    
    var directionsAPI : PXGoogleDirections!

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status == .authorizedWhenInUse{
            
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            let marker = GMSMarker(position: location.coordinate)
            marker.map = mapView
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            locationManager.stopUpdatingLocation()
        }
    }
    
    func reverseGeoCodeCoordinate(coordinate: CLLocationCoordinate2D){
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let address = response?.firstResult(){
                let lines = address.lines!
                self.addressLabel.text = lines.joined(separator: "\n")
                let labelHeight = self.addressLabel.intrinsicContentSize.height
                self.mapView.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0, bottom: labelHeight, right: 0)
                UIView.animate(withDuration: 0.25, animations:{
                    self.view.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    func showRoute() {
        APIConnection.printJsonArray(json: [directions])
        for (key, data) in directions{
            if key == "end_location"{
                let marker = GMSMarker(position: data as! CLLocationCoordinate2D)
                marker.map = mapView
                for (subKey, subData) in data as! [String : Any] {
                    print("\(subKey) : \(subData)")
                }
            }
        }
    }
    
    override func viewDidLoad() {
        let orderId = defaults.value(forKey: "orderId") as? Int
        
        for item in ModelController.getInstance().getOrderArray() {
            if (item.getOrderId() == orderId!) {
                order = item
            }
        }
        
        let addressLines = order.getDeliveryAddrsss().components(separatedBy: ", ")
        
        directionsAPI = PXGoogleDirections(apiKey: "AIzaSyB_iaajdIoZeqVM56ecblrIz_NJWj2ERMo", from: PXLocation.specificLocation("24 James Street", "Plymouth", "UK"), to: PXLocation.specificLocation(addressLines.first!, addressLines.last!, "UK"))
        
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        APIConnection.delegate = self
        APIConnection.getDirections(baseURLDirections)
        
        directionsAPI.calculateDirections({ response in
            switch response {
            case let .error(_, error):
                print(error.localizedDescription)
                break
            case let .success(request, routes):
                print(routes.count)
                for item in routes {
                    item.drawDestinationMarkerOnMap(self.mapView)
                    item.drawOnMap(self.mapView).strokeWidth = 5
                    
                }
                break
            }
        })
    }
    
    func receiveDirections(directions d : [String : Any]) {
        self.directions = d
        print("receiveDirections()")
        showRoute()
    }
}

extension GPSController: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeoCodeCoordinate(coordinate: position.target)
    }
}
